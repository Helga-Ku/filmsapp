package com.app.films.filmsapp.contentproviders;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.app.films.filmsapp.R;
import com.app.films.filmsapp.models.Category;
import com.app.films.filmsapp.models.Film;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by o.kusakina on 29.11.2015.
 */
public class FilmsOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "films.sqlite";
    private static final int DB_VERSION = 1;

    private Context mContext;
    private Resources mRes;

    public FilmsOpenHelper (Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DB_NAME, factory, DB_VERSION);
        mContext = context;
        mRes = context.getResources();
    }

    public FilmsOpenHelper (Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DB_NAME, factory, DB_VERSION, errorHandler);
        mContext = context;
        mRes = context.getResources();
    }

    @Override
    public void onCreate (SQLiteDatabase db) {
        db.execSQL(Contract.Category.CREATE);
        db.execSQL(Contract.Film.CREATE);
        fillDatabase(db);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void fillDatabase (SQLiteDatabase db) {
        db.delete(Contract.Category.TABLE, "1", null);
        db.delete(Contract.Film.TABLE, "1", null);

        final String[] categories = mRes.getStringArray(R.array.categories_names);
        final TypedArray drawables = mRes.obtainTypedArray(R.array.categories_previews);
        Category category;
        long id;
        for (int i = 0; i < categories.length; i++) {
            category = new Category(categories[i], resourceToUri(mContext, drawables.getResourceId(i, 0)));
            id = db.insert(Contract.Category.TABLE, null, category.toContentValues());
            Film film;
            final String[] films = mRes.getStringArray(getResourceId(mContext, "films_" + (i + 1), "array"));
            final TypedArray previews = mRes.obtainTypedArray(getResourceId(mContext, "films_previews_" + (i + 1), "array"));
            for (int j = 0; j < films.length; j++) {
                film = new Film(id, films[j], resourceToUri(mContext, previews.getResourceId(j, 0)));
                db.insert(Contract.Film.TABLE, null, film.toContentValues());
            }
        }
    }

    public static Uri resourceToUri (Context context, int resId) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resId) + '/' +
                context.getResources().getResourceTypeName(resId) + '/' +
                context.getResources().getResourceEntryName(resId));
    }

    public static int getResourceId (Context context, String variableName, String resourceName) {
        try {
            return context.getResources().getIdentifier(variableName, resourceName, context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
