package com.app.films.filmsapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;

import com.app.films.filmsapp.R;

import java.io.IOException;

/**
 * Created by o.kusakina on 28.11.2015.
 */
public class ImageUtils {

    private ImageUtils () {
    }

    public static Drawable getRoundedDrawable (Resources res, int resId) {
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(res, BitmapFactory.decodeResource(res, resId));
        drawable.setCornerRadius(res.getDimension(R.dimen.corner));
        return drawable;
    }

    public static Drawable getRoundedDrawable (Context context, Uri uri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
            drawable.setCornerRadius(context.getResources().getDimension(R.dimen.corner));
            return drawable;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
