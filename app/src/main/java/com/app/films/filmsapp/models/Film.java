package com.app.films.filmsapp.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.app.films.filmsapp.contentproviders.Contract;

/**
 * Created by o.kusakina on 28.11.2015.
 */
public class Film {
    private long mId;
    private long mCategoryId;
    private String mName;
    private Uri mPreview;

    public Film(){}

    public Film (long categoryId, String name, Uri preview) {
        mCategoryId = categoryId;
        mName = name;
        mPreview = preview;
    }

    public String getName () {
        return mName;
    }

    public Uri getPreview () {
        return mPreview;
    }

    public void read(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndex(Contract.Film._ID));
        mCategoryId = cursor.getLong(cursor.getColumnIndex(Contract.Film.CATEGORY_ID));
        mName = cursor.getString(cursor.getColumnIndex(Contract.Film.NAME));
        mPreview = Uri.parse(cursor.getString(cursor.getColumnIndex(Contract.Film.PREVIEW)));
    }

    public ContentValues toContentValues() {
        final ContentValues values = new ContentValues();
        values.put(Contract.Film.CATEGORY_ID, mCategoryId);
        values.put(Contract.Film.NAME, mName);
        values.put(Contract.Film.PREVIEW, mPreview.toString());
        return values;
    }
}
