package com.app.films.filmsapp;

import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.films.filmsapp.adapters.CategoriesAdapter;
import com.app.films.filmsapp.adapters.FilmsAdapter;
import com.app.films.filmsapp.contentproviders.Contract;
import com.app.films.filmsapp.models.Category;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String ARG_CATEGORY_ID = "arg_category_id";

    private ViewPager mCategoriesPager;
    private RecyclerView mFilmsRecycler;

    private CategoriesAdapter mCategoriesAdapter;
    private FilmsAdapter mFilmsAdapter;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCategoriesPager = (ViewPager) findViewById(R.id.categories);

        mFilmsRecycler = (RecyclerView) findViewById(R.id.filmsRecycler);
        mFilmsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mFilmsRecycler.setHasFixedSize(false);
        mFilmsAdapter = new FilmsAdapter(this);
        mFilmsRecycler.setAdapter(mFilmsAdapter);

        mCategoriesAdapter = new CategoriesAdapter(this, new ArrayList<Category>());
        mCategoriesPager.addOnPageChangeListener(new CategoriesListener());
        mCategoriesPager.addOnPageChangeListener(mCategoriesAdapter);
        mCategoriesPager.setClipChildren(false);
        mCategoriesPager.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mCategoriesPager.setAdapter(mCategoriesAdapter);
        mCategoriesPager.setCurrentItem(0);

        getSupportLoaderManager().initLoader(R.id.loader_categories, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader (int id, Bundle args) {
        switch (id) {
            case R.id.loader_categories:
                return new CursorLoader(this, Contract.Category.URI, null, null, null, null);
            case R.id.loader_films:
                return new CursorLoader(this, Contract.Film.URI, null, Contract.Film.CATEGORY_ID + "=?",
                        new String[]{String.valueOf(args.getLong(ARG_CATEGORY_ID))}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished (Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case R.id.loader_categories:
                mCategoriesAdapter.swap(data);
                Bundle args = new Bundle();
                args.putLong(ARG_CATEGORY_ID, mCategoriesAdapter.getItemAtPosition(mCategoriesPager.getCurrentItem()).getId());
                getSupportLoaderManager().restartLoader(R.id.loader_films, args, MainActivity.this);
                break;
            case R.id.loader_films:
                mFilmsAdapter.swap(data);
                break;
        }
    }

    @Override
    public void onLoaderReset (Loader<Cursor> loader) {

    }

    private class CategoriesListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled (int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected (int position) {
            Bundle args = new Bundle();
            args.putLong(ARG_CATEGORY_ID, mCategoriesAdapter.getItemAtPosition(position).getId());
            getSupportLoaderManager().restartLoader(R.id.loader_films, args, MainActivity.this);
        }

        @Override
        public void onPageScrollStateChanged (int state) {
        }
    }
}
