package com.app.films.filmsapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.films.filmsapp.R;
import com.app.films.filmsapp.models.Film;
import com.app.films.filmsapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by o.kusakina on 28.11.2015.
 */
public class FilmsAdapter extends RecyclerView.Adapter {

    private List<Film> mModels = new ArrayList<>();
    private final LayoutInflater mInflater;

    public FilmsAdapter (Context context) {
        super();
        mInflater = LayoutInflater.from(context);
    }


    public void setFilms (Collection<Film> films) {
        mModels.clear();
        mModels.addAll(films);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        return new FilmHolder(mInflater.inflate(R.layout.view_film, parent, false));
    }

    @Override
    public void onBindViewHolder (RecyclerView.ViewHolder holder, int position) {
        ((FilmHolder) holder).bind(mModels.get(position));
    }

    @Override
    public int getItemCount () {
        return mModels.size();
    }

    public void swap (Cursor cursor) {
        mModels.clear();
        Film film;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                film = new Film();
                film.read(cursor);
                mModels.add(film);
            } while (cursor.moveToNext());
        }
        notifyDataSetChanged();
    }

    private class FilmHolder extends RecyclerView.ViewHolder {

        private final TextView mNameTv;
        private final ImageView mPreviewIv;

        public FilmHolder (View itemView) {
            super(itemView);
            mNameTv = (TextView) itemView.findViewById(R.id.filmName);
            mPreviewIv = (ImageView) itemView.findViewById(R.id.filmPreview);
        }

        public void bind (Film film) {
            mNameTv.setText(film.getName());
            mPreviewIv.setImageDrawable(ImageUtils.getRoundedDrawable(mInflater.getContext(), film.getPreview()));
        }
    }
}
