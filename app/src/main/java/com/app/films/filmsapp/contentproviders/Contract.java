package com.app.films.filmsapp.contentproviders;

import android.net.Uri;

/**
 * Created by o.kusakina on 29.11.2015.
 */
public final class Contract {

    public static class Category implements Table {
        public static final String TABLE = "categories";
        public static final String NAME = "name";
        public static final String PREVIEW = "preview";
        public static final String CREATE = "CREATE TABLE " +
                TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                NAME + " TEXT, " +
                PREVIEW + " TEXT)";
        public static final Uri URI = Uri.withAppendedPath(FilmsProvider.URI, TABLE);
    }

    public static class Film implements Table {
        public static final String TABLE = "films";
        public static final String CATEGORY_ID = "category_id";
        public static final String NAME = "name";
        public static final String PREVIEW = "preview";
        public static final String CREATE = "CREATE TABLE " +
                TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                CATEGORY_ID + " INTEGER, " +
                NAME + " TEXT, " +
                PREVIEW + " TEXT)";
        public static final Uri URI = Uri.withAppendedPath(FilmsProvider.URI, TABLE);
    }

    public interface Table {
        public static final String _ID = "_id";
    }
}
