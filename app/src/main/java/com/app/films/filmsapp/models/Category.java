package com.app.films.filmsapp.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.app.films.filmsapp.contentproviders.Contract;

/**
 * Created by o.kusakina on 28.11.2015.
 */
public class Category {
    private long mId;
    private String mName;
    private Uri mPreview;

    public Category(){}

    public Category(String name, Uri preview) {
        mName = name;
        mPreview = preview;
    }

    public String getName () {
        return mName;
    }

    public Uri getPreview () {
        return mPreview;
    }

    public void read(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndex(Contract.Category._ID));
        mName = cursor.getString(cursor.getColumnIndex(Contract.Category.NAME));
        mPreview = Uri.parse(cursor.getString(cursor.getColumnIndex(Contract.Category.PREVIEW)));
    }

    public ContentValues toContentValues() {
        final ContentValues values = new ContentValues();
        values.put(Contract.Category.NAME, mName);
        values.put(Contract.Category.PREVIEW, mPreview.toString());
        return values;
    }

    public long getId () {
        return mId;
    }
}
