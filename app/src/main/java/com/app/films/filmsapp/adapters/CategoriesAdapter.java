package com.app.films.filmsapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.films.filmsapp.R;
import com.app.films.filmsapp.models.Category;
import com.app.films.filmsapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by o.kusakina on 28.11.2015.
 */
public class CategoriesAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

    private final List<Category> mModels = new ArrayList<>();
    private final LayoutInflater mInflater;
    private int mCurrent;

    public CategoriesAdapter (Context context, Collection<Category> models) {
        super();
        mInflater = LayoutInflater.from(context);
        mModels.addAll(models);
    }

    public Category getItemAtPosition (int position) {
        if (mModels.size() > getModelPosition(position)) {
            return mModels.get(getModelPosition(position));
        }
        return null;
    }

    @Override
    public int getCount () {
        return mModels.size();
    }

    @Override
    public boolean isViewFromObject (View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem (ViewGroup container, int position) {
        final Category model = mModels.get(getModelPosition(position));
        final ViewGroup group = (ViewGroup) mInflater.inflate(R.layout.view_category, container, false);
        ((TextView) group.findViewById(R.id.categoryName)).setText(model.getName());
        ((ImageView) group.findViewById(R.id.categoryPreview)).setImageDrawable(ImageUtils.getRoundedDrawable(mInflater.getContext(), model.getPreview()));
        group.setTag(model);
        container.addView(group, getModelPosition(position));
        return group;
    }

    @Override
    public void destroyItem (ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public void onPageScrolled (int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected (int position) {
        mCurrent = position;
    }

    @Override
    public void onPageScrollStateChanged (int state) {

    }

    private int getModelPosition (int position) {
        return position % mModels.size();
    }

    @Override
    public int getItemPosition (Object object) {
        return super.getItemPosition(object);
    }

    public void swap (Cursor cursor) {
        mModels.clear();
        Category category;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                category = new Category();
                category.read(cursor);
                mModels.add(category);
            } while (cursor.moveToNext());
        }
        notifyDataSetChanged();
    }
}
