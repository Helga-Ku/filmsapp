package com.app.films.filmsapp.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.net.URI;

/**
 * Created by o.kusakina on 29.11.2015.
 */
public class FilmsProvider extends ContentProvider {
    public static final String AUTHORITY = "com.app.films.filmsprovider";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY);

    private static final int CATEGORIES = 1;
    private static final int CATEGORY = 2;
    private static final int FILMS = 3;
    private static final int FILM = 4;

    private static final UriMatcher URI_MATCHER;
    private SQLiteOpenHelper mOpenHelper;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, Contract.Category.TABLE, CATEGORIES);
        URI_MATCHER.addURI(AUTHORITY, Contract.Category.TABLE + "/#", CATEGORY);
        URI_MATCHER.addURI(AUTHORITY, Contract.Film.TABLE, FILMS);
        URI_MATCHER.addURI(AUTHORITY, Contract.Film.TABLE + "/#", FILM);
    }

    @Override
    public boolean onCreate () {
        mOpenHelper = new FilmsOpenHelper(getContext(), null);
        return true;
    }

    @Nullable
    @Override
    public Cursor query (Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final String groupBy = null;
        final String having = null;
        final SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case CATEGORY:
                builder.appendWhere(Contract.Category._ID + "=" + getRowId(uri));
            case CATEGORIES:
                builder.setTables(Contract.Category.TABLE);
                break;
            case FILM:
                builder.appendWhere(Contract.Category._ID + "=" + getRowId(uri));
            case FILMS:
                builder.setTables(Contract.Film.TABLE);
                break;
        }
        return builder.query(db, projection, selection, selectionArgs, groupBy, having, sortOrder);
    }

    @Nullable
    @Override
    public String getType (Uri uri) {
        int match = URI_MATCHER.match(uri);
        switch (match) {
            case CATEGORIES:
                return "vnd.android.cursor.dir/" + Contract.Category.TABLE;
            case CATEGORY:
                return "vnd.android.cursor.item/" + Contract.Category.TABLE;
            case FILMS:
                return "vnd.android.cursor.dir/" + Contract.Film.TABLE;
            case FILM:
                return "vnd.android.cursor.item/" + Contract.Film.TABLE;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert (Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        String nullColumnHack = null;
        long id = db.insert(getTableByUri(uri), nullColumnHack, values);
        if (id > -1) {
            Uri insertedUri = getUri(uri, id);
            getContext().getContentResolver().notifyChange(insertedUri, null);
            return insertedUri;
        } else
            return null;
    }

    @Override
    public int delete (Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case CATEGORY:
            case FILM:
                selection = Contract.Table._ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
        }
        if (selection == null) selection = "1";
        int deleteCount = db.delete(getTableByUri(uri), selection, selectionArgs);
        if (deleteCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deleteCount;
    }

    @Override
    public int update (Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case CATEGORY:
            case FILM:
                selection = Contract.Table._ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
        }
        int updateCount = db.update(getTableByUri(uri), values, selection, selectionArgs);
        if (updateCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }

    public static String getRowId (Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static Uri getUri (Uri root, long id) {
        return ContentUris.withAppendedId(root, id);
    }

    public static String getTableByUri (Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CATEGORIES:
            case CATEGORY:
                return Contract.Category.TABLE;
            case FILMS:
            case FILM:
                return Contract.Film.TABLE;
        }
        return null;
    }
}
